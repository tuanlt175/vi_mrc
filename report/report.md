## Kết quả thử nghiệm các model


[Tải xuống các model đã training sẵn ở đây về và giải nén ra](https://drive.google.com/drive/folders/1grkZMS6j3lsbK-3GyiXNSgzBxz7QdC3z?usp=sharing)

[Tải xuống tập dữ liệu ViQuad 1.0 gốc ở đây](https://drive.google.com/drive/folders/1Amw1sBYFH6aXMVDuKgTRdFIlyocN6oLK?usp=sharing)

[Tải xuống tập dữ liệu ViQuad 1.0 đã tách từ sẵn bằng VNCoreNLP ở đây](https://drive.google.com/drive/folders/1Amw1sBYFH6aXMVDuKgTRdFIlyocN6oLK?usp=sharing) (Dữ liệu này dùng cho các model `phobert`). Nếu muốn thực hiện tách từ lại từ lại thì [Cài đặt và sử dụng VNCoreNLP theo hướng dẫn ở đây](https://github.com/VinAIResearch/PhoBERT#vncorenlp)

### Kết quả các model dạng Only Encoder
![Only Encoder MRC Model](./only_encoder_model.png)

| Pretrained Model (Huggingface card)   |  EM   |  F1   |                 Training Notes                              | Training Time |Best eval at Epoch|
|---------------------------------------|-------|-------|-------------------------------------------------------------|---------------|------------------|
|xlm-roberta-base                       | 62.16 | 80.42 |10 epoch, learning_rate=1e-5, batch_size=32, max_seq_len=512 |  280s/epoch   |         5        |
|xlm-roberta-large                      |`70.08`|`87.46`|10 epoch, learning_rate=1e-5, batch_size=12, max_seq_len=512 |  815s/epoch   |         2        |
|FPTAI/velectra-base-discriminator-cased| 60.07 | 79.62 |10 epoch, learning_rate=1e-5, batch_size=32, max_seq_len=512 |  305s/epoch   |         4        |
|vinai/phobert-base                     | 63.87 | 82.32 |10 epoch, learning_rate=1e-5, batch_size=32, max_seq_len=256 |  212s/epoch   |         5        |
|vinai/phobert-large                    | 66.41 | 83.88 |10 epoch, learning_rate=1e-5, batch_size=24, max_seq_len=256 |  365s/epoch   |         2        |
|microsoft/mdeberta-v3-base             | 60.85 | 80.50 |10 epoch, learning_rate=1e-5, batch_size=32, max_seq_len=512 |  1248s/epoch  |         4        |
|Fsoft-AIC/videberta-base               | 22.06 | 49.41 |10 epoch, learning_rate=1e-5, batch_size=32, max_seq_len=512 |  1198s/epoch  |         9        |


### Kết quả các model dạng Encoder-Decoder
![Encoder Decoder MRC Model](./encoder_decoder_model.png)

| Pretrained Model (Huggingface card)   |  EM   |  F1   |                 Training Notes                              | Training Time |Best eval at Epoch|
|---------------------------------------|-------|-------|-------------------------------------------------------------|---------------|------------------|
|VietAI/vit5-base                       | 55.93 | 75.91 |10 epoch, learning_rate=1e-5, batch_size=16, max_seq_len=512 |  428s/epoch   |         4        |
|VietAI/vit5-large                      | 62.12 | 82.65 |10 epoch, learning_rate=1e-5, batch_size=4,  max_seq_len=512 |  1450s/epoch  |         9        |
|vinai/bartpho-word-base                | 46.69 | 67.51 |10 epoch, learning_rate=1e-5, batch_size=16, max_seq_len=512 |  285s/epoch   |         10       |
|vinai/bartpho-word                     |       |       |10 epoch, learning_rate=1e-5, batch_size=4,  max_seq_len=512 |  1078s/epoch  |         10       |
