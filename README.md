# Vietnamese Machine Reading Comprehension
Mô hình đọc hiểu văn bản tiếng việt và trả lời câu hỏi

## Nội Dung
1. [Cài đặt](#setup) <br>
2. [Đào tạo model](#train_model) <br>
    2.1 [Dữ liệu](#training_data) <br>
    2.2 [Đào tạo model](#train_model_script) <br>
    2.3 [Đánh giá model](#evaluate_model) <br>
3. [Chạy Inference sử dụng Python Package](#inference) <br>

[Chi tiết kết quả thử nghiệm](./report/report.md)

## 1. Cài đặt <a name="setup"></a>
Yêu cầu môi trường:
``
python=3.7
conda==4.10.3
cuda=11.4
```

Cài đặt các thư viện python
```bash
conda create -n vimrc python=3.7 --yes
conda activate vimrc
pip install --user ipykernel
python -m ipykernel install --user --name=vimrc
pip install -r requirements.txt 
pip install -e .
```

## 2. Đào tạo model <a name="train_model"></a>
### 2.1 Dữ liệu <a name="training_data"></a>

Dữ liệu training ở dạng [SQuad2.0](https://rajpurkar.github.io/SQuAD-explorer/) <br>

### 2.2 Đào tạo model <a name="train_model_script"></a>
Thay đổi các tham số model_path, data_path, .. trong file `./run_train.sh`, sau đó mở terminal và chạy đoạn mã sau
```bash
chmod +x run_train.sh
./run_train.sh
```
Để biết thêm về các tham số, mở terminal và chạy `python vi_mrc/run_train.py --help`

### 2.3 Đánh giá model <a name="evaluate_model"></a>
Thay đổi các tham số model_path, data_path, .. trong file `./run_evaluate.sh`, sau đó mở terminal và chạy đoạn mã sau
```bash
chmod +x run_evaluate.sh
./run_evaluate.sh
```
Để biết thêm về các tham số, mở terminal và chạy `python vi_mrc/run_evaluate.py --help`


## 3. Chạy Inference sử dụng Python Package <a name="inference"></a>

```python
from vi_mrc import TFViMRC
```

