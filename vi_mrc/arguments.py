from vi_mrc.models_map import MODEL_VERSIONS
from dataclasses import dataclass, field
from typing import Optional


@dataclass
class ModelArguments:
    """Arguments pertaining to which model/config/tokenizer we are going to fine-tune from."""

    model_name_or_path: Optional[str] = field(
        metadata={
            "help": "Path to pretrained model or model identifier from huggingface.co/models"
        },
    )
    model_version: Optional[str] = field(
        metadata={
            "help": f"version name của model là một trong số {MODEL_VERSIONS}"
        },
    )


@dataclass
class DataTrainingArguments:
    """Arguments pertaining to what data we are going to input our model for training and eval."""

    data_train: str = field(metadata={"help": "The input data train"})
    data_dev: Optional[str] = field(default=None, metadata={"help": "The input data dev"})
    max_seq_len: int = field(
        default=256,
        metadata={
            "help": "The maximum total input sequence length after tokenization. Sequences longer "
            "than this will be truncated, sequences shorter will be padded."
        },
    )
    cache_data_size: int = field(
        default=5000,
        metadata={"help": "Data size cache of tf.dataset"},
    )
