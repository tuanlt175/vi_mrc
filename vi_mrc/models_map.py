"""Map tên model đến các lớp model và dataset tương ứng."""
from vi_mrc.models.tokenization_phobert_fast import PhobertTokenizerFast
from transformers import PreTrainedTokenizerFast, PreTrainedTokenizer, AutoTokenizer
from vi_mrc.models.roberta import TFRobertaMRCModel
from vi_mrc.models.xlmroberta import TFXLMRobertaMRCModel
from vi_mrc.models.electra import TFElectraMRCModel
from vi_mrc.models.deberta import TFDebertaV2MRCModel
from vi_mrc.models.bart import TFBartMRCModel
from vi_mrc.models.t5 import TFT5MRCModel
from vi_mrc.mydatasets import (
    MRCDataset,
    MRCDatasetForDeberta,
    MRCDatasetForSeq2SeqModel
)
from typing import Union


class VersionClassesMap():
    def __init__(
        self,
        model_class,
        dataset_class: MRCDataset,
        tokenizer_class: Union[PreTrainedTokenizerFast, PreTrainedTokenizer],
        description: str
    ):
        self.model_class = model_class
        self.dataset_class = dataset_class
        self.tokenizer_class = tokenizer_class
        self.description = description

    def __repr__(self) -> str:
        return self.description

    def __str__(self) -> str:
        return self.description


MODELS_MAP = {
    "phobert-mrc": VersionClassesMap(
        model_class=TFRobertaMRCModel,
        dataset_class=MRCDataset,
        tokenizer_class=PhobertTokenizerFast,
        description="Mô hình MRC cơ bản dựa trên PhoBERT model."
    ),
    "xlmroberta-mrc": VersionClassesMap(
        model_class=TFXLMRobertaMRCModel,
        dataset_class=MRCDataset,
        tokenizer_class=AutoTokenizer,
        description="Mô hình MRC cơ bản dựa trên XLM-RoBERTa model."
    ),
    "deberta-mrc": VersionClassesMap(
        model_class=TFDebertaV2MRCModel,
        dataset_class=MRCDatasetForDeberta,
        tokenizer_class=AutoTokenizer,
        description="Mô hình MRC cơ bản dựa trên DeBerta model."
    ),
    "electra-mrc": VersionClassesMap(
        model_class=TFElectraMRCModel,
        dataset_class=MRCDataset,
        tokenizer_class=AutoTokenizer,
        description="Mô hình MRC cơ bản dựa trên Electra model."
    ),
    "t5-mrc": VersionClassesMap(
        model_class=TFT5MRCModel,
        dataset_class=MRCDatasetForSeq2SeqModel,
        tokenizer_class=AutoTokenizer,
        description="Mô hình MRC cơ bản dựa trên T5 model."
    ),
    "bartpho-mrc": VersionClassesMap(
        model_class=TFBartMRCModel,
        dataset_class=MRCDatasetForSeq2SeqModel,
        tokenizer_class=AutoTokenizer,
        description="Mô hình MRC cơ bản dựa trên BART model."
    ),
}
MODEL_VERSIONS = list(MODELS_MAP.keys())


def get_model_version_classes(version_name: str) -> VersionClassesMap:
    """Trả về Model class và Dataset class tương ứng với version name."""
    if version_name not in MODELS_MAP:
        raise ValueError(f"version_name phải là 1 trong số {MODEL_VERSIONS}")
    else:
        return MODELS_MAP[version_name]
