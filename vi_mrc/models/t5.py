from transformers.models.t5.modeling_tf_t5 import (
    TFT5ForConditionalGeneration,
    TFSeq2SeqLMOutput
)
from tensorflow.python.keras.engine import data_adapter
import tensorflow as tf


class TFT5MRCModel(TFT5ForConditionalGeneration):
    def train_step(self, data):
        """
        A modification of Keras's default `train_step` that cleans up the printed metrics when we use a dummy loss. If
        a user specifies a loss at model compile time, this function behaves as the original Keras `train_step`. In
        this case, it expects the same `data` as the original function (i.e. `(inputs, labels)`).

        However, when the model is compiled without specifying the loss AND the expected label columns are passed as
        part of the input dictionary, the loss is computed internally (inside the model class) and is used in the
        backwards pass. In this case, `data` is a singleton tuple containing `(inputs,)`.

        This is possible under the aforementioned circumstances because our overriden compile function can set an
        additional loss function that reduces a `loss` output, and the model will output a `loss` component (notice the
        name matching) containing the loss that was used to train the pre-trained model.
        """
        # These are the only transformations `Model.fit` applies to user-input
        # data when a `tf.data.Dataset` is provided.
        data = data_adapter.expand_1d(data)
        x, y, sample_weight = data_adapter.unpack_x_y_sample_weight(data)
        # These next two lines differ from the base method - they avoid issues when the labels are in
        # the input dict (and loss is computed internally)
        if y is None and "labels" in x:
            y = x["labels"]  # Stops confusion with metric computations
        elif y is None and "input_ids" in x:
            # Just make any kind of dummy array to make loss work
            y = tf.zeros(tf.shape(x["input_ids"])[0], dtype=tf.int64)
        # Run forward pass.
        with tf.GradientTape() as tape:
            y_pred = self(x, training=True)
            loss = self.compiled_loss(y, y_pred, sample_weight, regularization_losses=self.losses)
        # Run backwards pass.
        self.optimizer.minimize(loss, self.trainable_variables, tape=tape)
        # When y_pred is a ModelOutput and y is a tf.Tensor the metrics update
        # should be done only with the relevant ModelOutput param that is
        # considered by the loss.
        if isinstance(y_pred, TFSeq2SeqLMOutput) and isinstance(y, tf.Tensor):
            y_pred = y_pred["logits"]
        self.compiled_metrics.update_state(y, y_pred, sample_weight)
        # Collect metrics to return
        return_metrics = {}
        for metric in self.metrics:
            result = metric.result()
            if isinstance(result, dict):
                return_metrics.update(result)
            else:
                return_metrics[metric.name] = result
        # These next two lines are also not in the base method - they correct the displayed metrics
        # when we're using a dummy loss, to avoid a bogus "loss_loss" value being shown.
        if "loss" in return_metrics and "loss_loss" in return_metrics:
            del return_metrics["loss_loss"]
        return return_metrics

    def test_step(self, data):
        """
        A modification of Keras's default test_step that cleans up the printed metrics when we use a dummy loss.
        """
        data = data_adapter.expand_1d(data)
        x, y, sample_weight = data_adapter.unpack_x_y_sample_weight(data)
        # These next two lines differ from the base method - they avoid issues when the labels are in
        # the input dict (and loss is computed internally)
        if y is None and "labels" in x:
            y = x["labels"]  # Stops confusion with metric computations
        elif y is None and "input_ids" in x:
            # Just make any kind of dummy array to make loss work
            y = tf.zeros(tf.shape(x["input_ids"])[0], dtype=tf.int64)
        y_pred = self(x, training=False)
        self.compiled_loss(y, y_pred, sample_weight, regularization_losses=self.losses)
        # Updates stateful loss metrics.
        if isinstance(y_pred, TFSeq2SeqLMOutput) and isinstance(y, tf.Tensor):
            y_pred = y_pred["logits"]
        self.compiled_metrics.update_state(y, y_pred, sample_weight)
        # Collect metrics to return
        return_metrics = {}
        for metric in self.metrics:
            result = metric.result()
            if isinstance(result, dict):
                return_metrics.update(result)
            else:
                return_metrics[metric.name] = result
        # These next two lines are also not in the base method - they correct the displayed metrics
        # when we're using a dummy loss, to avoid a bogus "loss_loss" value being shown.
        if "loss" in return_metrics and "loss_loss" in return_metrics:
            del return_metrics["loss_loss"]
        return return_metrics
