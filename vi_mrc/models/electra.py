from transformers.models.electra.modeling_tf_electra import (
    TFElectraMainLayer,
    TFQuestionAnsweringModelOutput,
    TFElectraForQuestionAnswering
)
from transformers.modeling_tf_outputs import TFSeq2SeqLMOutput
from transformers.modeling_tf_utils import (
    TFModelInputType,
    get_initializer,
    unpack_inputs,
)
from tensorflow.python.keras.engine import data_adapter
from typing import Optional, Tuple, Union
import tensorflow as tf


class TFElectraMRCModel(TFElectraForQuestionAnswering):
    def __init__(self, config, *inputs, **kwargs):
        super().__init__(config, *inputs, **kwargs)

        self.num_labels = config.num_labels
        self.electra = TFElectraMainLayer(config, name="electra")
        self.qa_outputs = tf.keras.layers.Dense(
            config.num_labels, kernel_initializer=get_initializer(config.initializer_range), name="qa_outputs"
        )

    @unpack_inputs
    def call(
        self,
        input_ids: TFModelInputType = None,
        attention_mask=None,
        token_type_ids=None,
        position_ids=None,
        head_mask=None,
        inputs_embeds=None,
        output_attentions: Optional[bool] = None,
        output_hidden_states: Optional[bool] = None,
        return_dict: Optional[bool] = None,
        labels=None,
        training: Optional[bool] = False,
    ) -> Union[TFQuestionAnsweringModelOutput, Tuple[tf.Tensor]]:
        r"""
        start_positions (`tf.Tensor` of shape `(batch_size,)`, *optional*):
            Labels for position (index) of the start of the labelled span for computing the token classification loss.
            Positions are clamped to the length of the sequence (`sequence_length`). Position outside of the sequence
            are not taken into account for computing the loss.
        end_positions (`tf.Tensor` of shape `(batch_size,)`, *optional*):
            Labels for position (index) of the end of the labelled span for computing the token classification loss.
            Positions are clamped to the length of the sequence (`sequence_length`). Position outside of the sequence
            are not taken into account for computing the loss.
        """
        discriminator_hidden_states = self.electra(
            input_ids=input_ids,
            attention_mask=attention_mask,
            token_type_ids=token_type_ids,
            position_ids=position_ids,
            head_mask=head_mask,
            inputs_embeds=inputs_embeds,
            output_attentions=output_attentions,
            output_hidden_states=output_hidden_states,
            return_dict=return_dict,
            training=training,
        )
        discriminator_sequence_output = discriminator_hidden_states[0]
        logits = self.qa_outputs(discriminator_sequence_output)
        start_logits, end_logits = tf.split(logits, 2, axis=-1)
        start_logits = tf.squeeze(start_logits, axis=-1)
        end_logits = tf.squeeze(end_logits, axis=-1)
        loss = None

        loss = None
        if labels is not None:
            loss = self.hf_compute_loss(labels, (start_logits, end_logits))

        if not return_dict:
            output = (
                start_logits,
                end_logits,
            ) + discriminator_hidden_states[1:]

            return ((loss,) + output) if loss is not None else output

        return TFQuestionAnsweringModelOutput(
            loss=loss,
            start_logits=start_logits,
            end_logits=end_logits,
            hidden_states=discriminator_hidden_states.hidden_states,
            attentions=discriminator_hidden_states.attentions,
        )

    def hf_compute_loss(self, labels, logits):
        loss_fn = tf.keras.losses.SparseCategoricalCrossentropy(
            from_logits=True, reduction=tf.keras.losses.Reduction.NONE
        )
        start_loss = loss_fn(labels[:, 0], logits[0])
        end_loss = loss_fn(labels[:, 1], logits[1])

        return (start_loss + end_loss) / 2.0

    def train_step(self, data):
        """
        A modification of Keras's default `train_step` that cleans up the printed metrics when we use a dummy loss. If
        a user specifies a loss at model compile time, this function behaves as the original Keras `train_step`. In
        this case, it expects the same `data` as the original function (i.e. `(inputs, labels)`).

        However, when the model is compiled without specifying the loss AND the expected label columns are passed as
        part of the input dictionary, the loss is computed internally (inside the model class) and is used in the
        backwards pass. In this case, `data` is a singleton tuple containing `(inputs,)`.

        This is possible under the aforementioned circumstances because our overriden compile function can set an
        additional loss function that reduces a `loss` output, and the model will output a `loss` component (notice the
        name matching) containing the loss that was used to train the pre-trained model.
        """
        # These are the only transformations `Model.fit` applies to user-input
        # data when a `tf.data.Dataset` is provided.
        data = data_adapter.expand_1d(data)
        x, y, sample_weight = data_adapter.unpack_x_y_sample_weight(data)
        # These next two lines differ from the base method - they avoid issues when the labels are in
        # the input dict (and loss is computed internally)
        if y is None and "labels" in x:
            y = x["labels"]  # Stops confusion with metric computations
        elif y is None and "input_ids" in x:
            # Just make any kind of dummy array to make loss work
            y = tf.zeros(tf.shape(x["input_ids"])[0], dtype=tf.int64)
        # Run forward pass.
        with tf.GradientTape() as tape:
            y_pred = self(x, training=True)
            loss = self.compiled_loss(y, y_pred, sample_weight, regularization_losses=self.losses)
        # Run backwards pass.
        self.optimizer.minimize(loss, self.trainable_variables, tape=tape)
        # When y_pred is a ModelOutput and y is a tf.Tensor the metrics update
        # should be done only with the relevant ModelOutput param that is
        # considered by the loss.
        if isinstance(y_pred, TFSeq2SeqLMOutput) and isinstance(y, tf.Tensor):
            y_pred = y_pred["logits"]
        self.compiled_metrics.update_state(y, y_pred, sample_weight)
        # Collect metrics to return
        return_metrics = {}
        for metric in self.metrics:
            result = metric.result()
            if isinstance(result, dict):
                return_metrics.update(result)
            else:
                return_metrics[metric.name] = result
        # These next two lines are also not in the base method - they correct the displayed metrics
        # when we're using a dummy loss, to avoid a bogus "loss_loss" value being shown.
        if "loss" in return_metrics and "loss_loss" in return_metrics:
            del return_metrics["loss_loss"]
        return return_metrics

    def test_step(self, data):
        """
        A modification of Keras's default test_step that cleans up the printed metrics when we use a dummy loss.
        """
        data = data_adapter.expand_1d(data)
        x, y, sample_weight = data_adapter.unpack_x_y_sample_weight(data)
        # These next two lines differ from the base method - they avoid issues when the labels are in
        # the input dict (and loss is computed internally)
        if y is None and "labels" in x:
            y = x["labels"]  # Stops confusion with metric computations
        elif y is None and "input_ids" in x:
            # Just make any kind of dummy array to make loss work
            y = tf.zeros(tf.shape(x["input_ids"])[0], dtype=tf.int64)
        y_pred = self(x, training=False)
        self.compiled_loss(y, y_pred, sample_weight, regularization_losses=self.losses)
        # Updates stateful loss metrics.
        if isinstance(y_pred, TFSeq2SeqLMOutput) and isinstance(y, tf.Tensor):
            y_pred = y_pred["logits"]
        self.compiled_metrics.update_state(y, y_pred, sample_weight)
        # Collect metrics to return
        return_metrics = {}
        for metric in self.metrics:
            result = metric.result()
            if isinstance(result, dict):
                return_metrics.update(result)
            else:
                return_metrics[metric.name] = result
        # These next two lines are also not in the base method - they correct the displayed metrics
        # when we're using a dummy loss, to avoid a bogus "loss_loss" value being shown.
        if "loss" in return_metrics and "loss_loss" in return_metrics:
            del return_metrics["loss_loss"]
        return return_metrics

    @tf.function(
        input_signature=[
            tf.TensorSpec(shape=(None, None), dtype=tf.int32, name="input_ids"),
            tf.TensorSpec(shape=(None, None), dtype=tf.int32, name="attention_mask")
        ]
    )
    def infer(
        self,
        input_ids: tf.Tensor,
        attention_mask: tf.Tensor
    ):
        outputs = self.electra(
            input_ids,
            attention_mask=attention_mask,
            output_attentions=None,
            output_hidden_states=None,
            return_dict=None,
            training=False,
        )
        sequence_output = outputs[0]
        logits = self.qa_outputs(sequence_output)
        start_logits, end_logits = tf.split(logits, 2, axis=-1)
        start_logits = tf.squeeze(start_logits, axis=-1)
        end_logits = tf.squeeze(end_logits, axis=-1)
        return start_logits, end_logits
