from tensorflow.python.keras.callbacks import Callback
import numpy as np
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class SavePretrainedPerNSteps(Callback):
    """SavePretrainedPerNSteps class."""

    # Hugging Face models have a save_pretrained() method that saves both the weights and the necessary
    # metadata to allow them to be loaded as a pretrained model in future. This is a simple Keras callback
    # that saves the model with this method after each epoch.
    def __init__(self, output_dir, save_freq, **kwargs):
        """Constructor function."""
        super().__init__()
        self.output_dir = output_dir
        self.save_freq = save_freq

    def _save_model(self, epoch, logs):
        """Save model every n steps."""
        self.model.save_pretrained(self.output_dir)

    def on_train_batch_end(self, batch, logs=None):
        """On train batch end."""
        if self.save_freq > 0 and batch != 0 and batch % self.save_freq == 0:
            logger.warning(
                f"\nPretrained model was saved in {self.output_dir}. Training in batch: {batch}"
            )
            self.model.save_pretrained(self.output_dir)

    def on_epoch_end(self, epoch, logs=None):
        """on epoch end."""
        logger.warning("\n")
        self.model.save_pretrained(self.output_dir)


class SaveBestModel(Callback):
    """SaveBestModel Callback class."""

    # Hugging Face models have a save_pretrained() method that saves both the weights and the necessary
    # metadata to allow them to be loaded as a pretrained model in future. This is a simple Keras callback
    # that saves the model with this method after each epoch.
    def __init__(self, output_dir, **kwargs):
        """Constructor function."""
        super().__init__()
        self.best_model_path = str(output_dir) + "-best-val"
        self.best_val_loss = np.inf

    def on_test_end(self, logs=None):
        """on epoch end."""
        if "loss" in logs:
            if self.best_val_loss >= logs["loss"]:
                self.best_val_loss = logs["loss"]
                logger.warning(
                    f"\nBest model was saved in {self.best_model_path}. Validation loss: {self.best_val_loss}"
                )
                self.model.save_pretrained(self.best_model_path)
