from transformers.models.xlm_roberta.modeling_tf_xlm_roberta import (
    TFXLMRobertaForQuestionAnswering,
    TFXLMRobertaMainLayer
)
from transformers.modeling_tf_utils import get_initializer
import tensorflow as tf
from transformers.models.xlm_roberta.configuration_xlm_roberta import XLMRobertaConfig
from vi_mrc.models.roberta import TFRobertaMRCModel


class TFXLMRobertaMRCModel(TFRobertaMRCModel):
    _keys_to_ignore_on_load_unexpected = [r"pooler", r"lm_head"]
    config_class = XLMRobertaConfig
    base_model_prefix = "roberta"

    def __init__(self, config, *inputs, **kwargs):
        super().__init__(config, *inputs, **kwargs)
        self.num_labels = config.num_labels

        self.roberta = TFXLMRobertaMainLayer(config, add_pooling_layer=False, name="roberta")
        self.qa_outputs = tf.keras.layers.Dense(
            config.num_labels, kernel_initializer=get_initializer(config.initializer_range), name="qa_outputs"
        )
