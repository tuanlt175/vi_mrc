import logging
import os
import sys
from pathlib import Path
import tensorflow as tf
import transformers
from transformers import (
    HfArgumentParser,
    TFTrainingArguments,
    set_seed,
    CONFIG_NAME,
    TF2_WEIGHTS_NAME
)
from transformers.optimization_tf import create_optimizer
from vi_mrc.arguments import ModelArguments, DataTrainingArguments
from vi_mrc.models.callbacks import SavePretrainedPerNSteps, SaveBestModel
from vi_mrc.models_map import get_model_version_classes


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def main():
    # region Argument Parsing
    parser = HfArgumentParser(
        (ModelArguments, DataTrainingArguments, TFTrainingArguments))
    if len(sys.argv) == 2 and sys.argv[1].endswith(".json"):
        # If we pass only one argument to the script and it's the path to a json file,
        # let's parse it to get our arguments.
        model_args, data_args, training_args = parser.parse_json_file(
            json_file=os.path.abspath(sys.argv[1]))
    else:
        model_args, data_args, training_args = parser.parse_args_into_dataclasses()

    # Fix multi GPU training strategy
    if len(tf.config.list_physical_devices("GPU")) > 1:
        training_strategy = tf.distribute.experimental.MultiWorkerMirroredStrategy()
    else:
        training_strategy = training_args.strategy
    logger.warning(str(training_strategy))

    # Sanity checks
    if training_args.output_dir is not None:
        training_args.output_dir = Path(training_args.output_dir)
        os.makedirs(training_args.output_dir, exist_ok=True)
    # endregion

    # get model class, dataset class, config class, tokenizer class, ...
    model_version_classes = get_model_version_classes(model_args.model_version)

    # region Setup logging
    # accelerator.is_local_main_process is only True for one process per machine.
    logger.setLevel(logging.INFO)
    transformers.utils.logging.set_verbosity_info()
    # endregion

    # If passed along, set the training seed now.
    if training_args.seed is not None:
        logger.warning(f"Setting Random seed={training_args.seed} !!!!!")
        set_seed(training_args.seed)

    config = model_version_classes.model_class.config_class.from_pretrained(model_args.model_name_or_path)
    config.num_labels = 2
    tokenizer = model_version_classes.tokenizer_class.from_pretrained(
        model_args.model_name_or_path, do_lower_case=False, strip_accents=False
    )

    # data region
    # region Load datasets
    train_dataset = model_version_classes.dataset_class(
        data_file_path=data_args.data_train,
        tokenizer=tokenizer,
        max_seq_len=data_args.max_seq_len
    )
    if data_args.data_dev is not None:
        eval_dataset = model_version_classes.dataset_class(
            data_file_path=data_args.data_dev,
            tokenizer=tokenizer,
            max_seq_len=data_args.max_seq_len
        )
    else:
        eval_dataset = None
    # endregion

    with training_strategy.scope():
        # region Prepare model
        model = model_version_classes.model_class.from_pretrained(model_args.model_name_or_path, config=config)
        model.config.model_version = model_args.model_version

        # endregion

        # region TF Dataset preparation
        num_replicas = training_strategy.num_replicas_in_sync

        tf_train_dataset = train_dataset.to_tf_dataset(
            num_train_epochs=training_args.num_train_epochs,
            batch_size=num_replicas * training_args.per_device_train_batch_size,
            cache_size=data_args.cache_data_size,
            drop_remainder=False
        )
        if eval_dataset is not None:
            tf_eval_dataset = eval_dataset.to_tf_dataset(
                num_train_epochs=1,
                batch_size=num_replicas * training_args.per_device_eval_batch_size
            )
        else:
            tf_eval_dataset = None
        # endregion

        # region Training and validation
        logger.warning("***** Running training *****")
        logger.warning(f"  Num examples = {len(train_dataset)}")
        logger.warning(f"  Num Epochs = {training_args.num_train_epochs}")
        logger.warning(f"  Learning rate = {training_args.learning_rate}")
        logger.warning(
            f"  Instantaneous batch size per device = {training_args.per_device_train_batch_size}")
        logger.warning(
            f"  Total train batch size = {num_replicas * training_args.per_device_train_batch_size}")

        # Bias and layernorm weights are automatically excluded from the decay
        batches_per_epoch = len(train_dataset) // (
            num_replicas * training_args.per_device_train_batch_size
        )

        optimizer, _ = create_optimizer(
            init_lr=training_args.learning_rate,
            num_train_steps=int(training_args.num_train_epochs * batches_per_epoch),
            num_warmup_steps=training_args.warmup_steps,
            adam_beta1=training_args.adam_beta1,
            adam_beta2=training_args.adam_beta2,
            adam_epsilon=training_args.adam_epsilon,
            weight_decay_rate=training_args.weight_decay,
        )

        # no user-specified loss = will use the model internal loss
        # model.compile(optimizer=optimizer, run_eagerly=True)
        def dummy_loss(y_true, y_pred):
            return tf.reduce_mean(y_pred)

        model.compile(loss={"loss": dummy_loss}, optimizer=optimizer)

        # model fit
        history = model.fit(
            tf_train_dataset,
            validation_data=tf_eval_dataset,
            epochs=int(training_args.num_train_epochs),
            steps_per_epoch=batches_per_epoch,
            callbacks=[
                SavePretrainedPerNSteps(
                    output_dir=training_args.output_dir,
                    save_freq=training_args.save_steps
                ),
                SaveBestModel(training_args.output_dir)
            ],
        )

        logger.warning(f"  Final train loss: {history.history['loss'][-1]:.3f}")
        if "val_loss" in history.history:
            logger.warning(f"  Final validation loss: {history.history['val_loss'][-1]:.3f}")

        # save model
        model.save_pretrained(training_args.output_dir)
        tokenizer.save_pretrained(training_args.output_dir)
        if tf_eval_dataset is not None:
            tokenizer.save_pretrained(str(training_args.output_dir).rstrip("/") + "-best-val")
        # endregion

    if training_args.push_to_hub:
        # You'll probably want to append some of your own metadata here!
        model.push_to_hub()


if __name__ == "__main__":
    main()
