from vi_mrc import TFViMRC
import collections
import string
import json
import argparse
import tqdm
import numpy as np
import os


def normalize_answer(s):
    """Lower text and remove punctuation, articles and extra whitespace."""

    def white_space_fix(text):
        return ' '.join(text.split())

    def remove_punc(text):
        exclude = set(string.punctuation)
        return ''.join(ch for ch in text if ch not in exclude)

    def lower(text):
        return text.lower()
    s = s.replace("_", " ")
    return white_space_fix(remove_punc(lower(s)))


def get_tokens(s):
    if not s:
        return []
    return normalize_answer(s).split()


def compute_exact(a_gold, a_pred):
    return int(normalize_answer(a_gold) == normalize_answer(a_pred))


def compute_f1(a_gold, a_pred):
    gold_toks = get_tokens(a_gold)
    pred_toks = get_tokens(a_pred)
    common = collections.Counter(gold_toks) & collections.Counter(pred_toks)
    num_same = sum(common.values())
    if len(gold_toks) == 0 or len(pred_toks) == 0:
        # If either is no-answer, then F1 is 1 if they agree, 0 otherwise
        return int(gold_toks == pred_toks)
    if num_same == 0:
        return 0
    precision = 1.0 * num_same / len(pred_toks)
    recall = 1.0 * num_same / len(gold_toks)
    f1 = (2 * precision * recall) / (precision + recall)
    return f1


def main():
    """Hàm chính để chạy đánh giá model."""
    parser = argparse.ArgumentParser(allow_abbrev=False)
    parser.add_argument(
        "--model_path",
        "-m",
        type=str,
        help="Đường dẫn đến thư mục chứa model",
    )
    parser.add_argument(
        "--data_path",
        "-d",
        type=str,
        help="Đường dẫn file json chứa dữ liệu test.",
    )
    parser.add_argument(
        "--batch_size",
        "-b",
        default=16,
        type=int,
        help="Kích thước lô.",
    )
    parser.add_argument(
        "--max_seq_len",
        default=512,
        type=int,
        help="Số token tối đa của một chuỗi đầu vào.",
    )
    parser.add_argument(
        "--output_file",
        default="model_outputs.json",
        type=str,
        help="File chứa kết quả của models",
    )
    args = parser.parse_args()
    model = TFViMRC(model_path=args.model_path, max_seq_len=args.max_seq_len)

    data = model.dataset_obj.read_data(args.data_path)

    print("-------- Run model infer !!! --------")
    for start in tqdm.tqdm(range(0, len(data), args.batch_size)):
        end = min(start + args.batch_size, len(data))
        answers = model.batch_infer(data[start:end])
        for item, answer in zip(data[start:end], answers):
            item["model_answer"] = answer

    print("-------- Compute Metrics !!! --------")
    for x in data:
        x["em"] = max([compute_exact(a, x["model_answer"]) for a in x["all_answers"]])
        x["f1"] = max([compute_f1(a, x["model_answer"]) for a in x["all_answers"]])
        if "answer" in x:
            del x["answer"]
    total_data = len(data)
    EM = sum([x["em"] for x in data]) / total_data
    F1 = sum([x["f1"] for x in data]) / total_data
    print(f"Total data: {total_data}")
    print(f"EM        : {EM}")
    print(f"F1        : {F1}")
    with open(args.output_file, "w") as file:
        json.dump(
            {
                "results": {
                    "total_sample": total_data,
                    "EM": EM,
                    "F1": F1,
                    "model_path": args.model_path,
                },
                "data": data,
            }, file, ensure_ascii=False, indent=4
        )


if __name__ == "__main__":
    main()
