import copy
import itertools
import re
import unicodedata as ud


__specials__ = [r"==>", r"->", r"\.\.\.", r">>"]
__digit__ = r"\d+([\.,_]\d+)+"
__email__ = r"([a-zA-Z0-9_.+-]+@([a-zA-Z0-9-]+\.)+[a-zA-Z0-9-]+)"
__web__ = r"\w+://[^\s]+"
__word__ = r"\w+"
__non_word__ = r"[^\w\s]"
__abbreviations__ = [
    r"[A-ZĐ]+\.",
    r"[Tt]p\.",
    r"[Mm]r\.",
    r"[Mm]rs\.",
    r"[Mm]s\.",
    r"[Dd]r\.",
    r"[Tt]h[sS]\.",
]

__PATTERNS__ = (
    __abbreviations__
    + __specials__
    + [__web__, __email__, __digit__, __non_word__, __word__]
)
__REGEX_PATTERNS__ = "(" + "|".join(__PATTERNS__) + ")"


def normalize_unicode(text):
    """Hàm chuẩn hóa dữ liệu, xóa các khoảng trắng thừa đi."""
    text = ud.normalize("NFC", text)
    return text


def normalize(text):
    """Hàm chuẩn hóa dữ liệu, xóa các khoảng trắng thừa đi."""
    text = ud.normalize("NFC", text)
    return " ".join(text.split())


def split_data(data, sep_token="</s>", max_len=256):
    """Chia các document dài thành nhiều phần khác nhau."""
    all_data = []
    for document in data:
        all_data += split_document(document, sep_token, max_len)
    return all_data


def split_document(document, sep_token="</s>", max_len=256):
    """Chia document dài thành nhiều phần khác nhau."""
    parts = []
    tmp_sentences = []
    sum_len = 0
    prev_sentence = []

    def _get_len(tmp_sentences):
        return (
            sum([len(sent["text"].split()) for sent in tmp_sentences])
            + len(tmp_sentences)
            - 1
        )

    for sentence in document["data"]:
        length = len(sentence["text"].split())
        prev_sentence = prev_sentence[-2:] + [sentence]
        if sum_len + length < max_len:
            tmp_sentences.append(sentence)
            sum_len += length + 1
        else:
            if tmp_sentences:
                parts.append(tmp_sentences)
            while True:
                tmp_sentences = copy.deepcopy(prev_sentence)
                sum_len = _get_len(tmp_sentences)
                if sum_len <= max_len or len(tmp_sentences) == 1:
                    break
                else:
                    prev_sentence = prev_sentence[1:]
    if tmp_sentences:
        parts.append(tmp_sentences)

    parts = [merge_sentence(sentences, sep_token) for sentences in parts]
    return parts


def merge_sentence(sentences, sep_token="</s>"):
    texts = []
    entities = []
    add_index = 0
    domain = sentences[0]["domain"]
    for line in sentences:
        texts.append(line["text"])
        for ent in line["entities"]:
            ent2 = copy.deepcopy(ent)
            ent2["start"] += add_index
            ent2["end"] += add_index
            entities.append(ent2)
        add_index += len(line["text"].split()) + 1
    output = {
        "text": f" {sep_token} ".join(texts),
        "entities": entities,
        "domain": domain,
    }

    def check_output(output):
        _texts = output["text"].split()
        for ent in output["entities"]:
            if ent["value"] != " ".join(_texts[ent["start"]: ent["end"]]):
                raise ValueError("Lỗi không khớp entities khi merge câu.")

    check_output(output)
    return output


def flatten_list(l):
    """Làm phẳng một danh sách hai chiều không đồng nhất, ví dụ:
    [[1,2,3,4], [1,3], [5], [6,7,8]] --> [1, 2, 3, 4, 1, 3, 5, 6, 7, 8]
    """
    return list(itertools.chain.from_iterable(l))


def sylabelize(text):
    """Hàm tách các dấu câu đặc biệt ra khỏi chữ cái.

    Ví dụ: "Tuấn." -> "Tuấn ."
    """
    text = ud.normalize("NFC", text)
    tokens = re.findall(__REGEX_PATTERNS__, text, re.UNICODE)

    return " ".join([token[0] for token in tokens])


def remove_accents(s):
    """Xóa dấu của một câu.

    Ví dụ: Xóa dấu -> Xoa dau
    """
    s = re.sub("Đ", "D", s)
    s = re.sub("đ", "d", s)
    s = ud.normalize("NFKD", s).encode("ASCII", "ignore").decode("utf-8")
    return s


def check_entity_index(sentence):
    words = sentence["text"].split()
    for ent in sentence["entities"]:
        if ent["value"] != " ".join(words[ent["start"]: ent["end"]]):
            print(ent["value"])
            print(" ".join(words[ent["start"]: ent["end"]]))
            raise ValueError(f"Lỗi không khớp index: {sentence}")
    return


ALL_CHAR_ACCENTS = "áàảãạăắằẳẵặâấầẩẫậéèẻẽẹêếềểễệíìỉĩịóòỏõọôốồổỗộơớờởỡợúùủũụưứừửữựýỳỷỹỵđ"


def sentence_tokenize(context):
    """Tách câu."""
    def refine_sentences(sentences):
        if len(sentences) > 1:
            for i, s in enumerate(sentences[:-1]):
                if re.fullmatch(r"[0-9a-zA-Z]{1,4}\.", s):
                    sentences[i + 1] = s + " " + sentences[i + 1]
                    sentences[i] = ""

        sentences = [s for s in sentences if s != ""]

        if len(sentences) > 1:
            for i in range(1, len(sentences)):
                if len(sentences[i]) < 5:
                    sentences[i - 1] = sentences[i - 1] + " " + sentences[i]
                    sentences[i] = ""
        return [s for s in sentences if s != ""]

    vi_alpha = f"[A-Z{ALL_CHAR_ACCENTS.upper()}]"
    vi_alpha_lower = vi_alpha.lower()
    split_regex = f'(?<![A-Z][a-z]\.)(?<!{vi_alpha}\.)'
    split_regex += r'(?<=\.)\s'
    split_regex += f'(?![0-9])(?!{vi_alpha_lower})'
    split_regex += r'(?![\(])(?![\{])(?![\[])'
    contexts = [re.split(split_regex, s) for s in context.splitlines()]
    contexts = [refine_sentences(sentences) for sentences in contexts]
    contexts = flatten_list(contexts)
    return contexts
