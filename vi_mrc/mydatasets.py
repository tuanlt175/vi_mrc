from typing import Union, Optional, List
import numpy as np
import tensorflow as tf
from transformers import PreTrainedTokenizer, PreTrainedTokenizerFast
from vi_mrc.utils import normalize_unicode
import json


class MRCDataset:
    def __init__(
        self,
        data_file_path: str,
        tokenizer: Union[PreTrainedTokenizer, PreTrainedTokenizerFast],
        pad_token_label_id: int = -100,
        max_seq_len: int = 256,
        **kwargs,
    ):
        self.tokenizer = tokenizer
        self.pad_token_label_id = pad_token_label_id
        self.max_seq_len = max_seq_len
        if data_file_path is None:
            self._data = []
        else:
            self._data = self.read_data(data_file_path)
        self.num_examples = len(self._data)

    def read_data(self, json_data_file):
        with open(json_data_file, "r") as file:
            raw_data = json.load(file)
        raw_data = raw_data["data"]
        data = []
        for wiki_page in raw_data:
            for paragraph in wiki_page["paragraphs"]:
                for qa in paragraph["qas"]:
                    data.append({
                        "title": normalize_unicode(wiki_page["title"]),
                        "content": normalize_unicode(paragraph["context"]),
                        "question": normalize_unicode(qa["question"]),
                        # Chỉ lấy câu trả lời đầu tiên train model
                        "answer": normalize_unicode(qa["answers"][0]["text"]),
                        "answer_start": qa["answers"][0]["answer_start"],
                        "all_answers": [normalize_unicode(a["text"]) for a in qa["answers"]],
                        "id": qa["id"],
                    })
        # Một số mẫu dữ liệu bị sai lệch answer_start, cần xác định lại
        for i, x in enumerate(data):
            start = x["answer_start"]
            len_answer = len(x["answer"])
            found = False
            for s in range(start - 5, start + 5):
                if x["answer"] == x["content"][s:s + len_answer]:
                    x["answer_start"] = s
                    found = True
                    break
            if not found:
                # Nếu không thể xác định answer_start, xóa mẫu dữ liệu đó
                data[i] = None
        data = [x for x in data if x is not None]
        return data

    def __len__(self):
        return self.num_examples

    def generator(self):
        """Generator."""
        for sample in self._data:
            yield self._convert_sample_for_training(sample)

    @staticmethod
    def get_output_types():
        output_types = {
            "input_ids": tf.int32,
            "attention_mask": tf.int32,
            "labels": tf.int32,
        }
        output_shapes = {
            "input_ids": tf.TensorShape([None]),
            "attention_mask": tf.TensorShape([None]),
            "labels": tf.TensorShape([None]),
        }
        return output_types, output_shapes

    def to_tf_dataset(self, num_train_epochs=1, batch_size=16, cache_size=5000, drop_remainder=False):
        output_types, output_shapes = self.get_output_types()
        options = tf.data.Options()
        options.experimental_distribute.auto_shard_policy = (
            tf.data.experimental.AutoShardPolicy.OFF
        )
        tf_dataset = tf.data.Dataset.from_generator(
            self.generator,
            output_types=output_types,
            output_shapes=output_shapes,
        )
        tf_dataset = tf_dataset.apply(
            tf.data.experimental.assert_cardinality(self.num_examples)
        )
        tf_dataset = (
            tf_dataset
            .with_options(options)
            .shuffle(cache_size)
            .batch(
                batch_size=batch_size,
                drop_remainder=drop_remainder,
            )
            .repeat(int(num_train_epochs))
        )
        return tf_dataset

    def _convert_sample_for_training(self, sample: dict):
        title = sample["title"]
        question = sample["question"]
        content = sample["content"]

        context = f"Câu hỏi: {question}. Tiêu đề: {title}. Nội dung: "
        answer_start = sample["answer_start"] + len(context)
        answer_end = answer_start + len(sample["answer"])
        context += content

        inputs = self.tokenizer(
            context,
            truncation=True,
            max_length=self.max_seq_len,
            padding="max_length",
            return_offsets_mapping=True
        )
        start_position = None
        end_position = None
        for i, offset in enumerate(inputs["offset_mapping"]):
            if offset[0] == offset[1] and i > 0:
                break
            if offset[0] >= answer_start and start_position is None:
                start_position = i
            if offset[1] >= answer_end and end_position is None:
                end_position = i
        if start_position is None:
            start_position, end_position = 0, 0
        elif end_position is None:
            end_position = self.max_seq_len - 1
        return {
            "input_ids": inputs["input_ids"],
            "attention_mask": inputs["attention_mask"],
            "labels": [start_position, end_position],
        }

    def _convert_sample_for_infer(self, sample: dict):
        title = sample["title"]
        question = sample["question"]
        content = sample["content"]

        context = f"Câu hỏi: {question}. Tiêu đề: {title}. Nội dung: "
        prefix_len = len(context)
        context += content

        inputs = self.tokenizer(
            context,
            truncation=True,
            max_length=self.max_seq_len,
            padding="max_length",
            return_offsets_mapping=True
        )
        for i, offset in enumerate(inputs["offset_mapping"]):
            if offset[0] == offset[1] and i > 0:
                inputs["offset_mapping"][i] = None
            if offset[0] < prefix_len:
                inputs["offset_mapping"][i] = None
        offset_mapping = {
            "text": context,
            "offset_mapping": inputs["offset_mapping"]
        }
        return inputs["input_ids"], inputs["attention_mask"], offset_mapping

    def batch_get_inputs_for_generation(self, data: List[dict]):
        inputs = [
            self._convert_sample_for_infer(sample) for sample in data
        ]
        input_ids = tf.constant([x[0] for x in inputs], tf.int32)
        attention_mask = tf.constant([x[1] for x in inputs], tf.int32)
        offset_mapping = [x[2] for x in inputs]
        return {
            "input_ids": input_ids,
            "attention_mask": attention_mask,
        }, offset_mapping

    def batch_decode_model_outputs(self, start_scores, end_scores, offset_mapping):
        contexts = [x["text"] for x in offset_mapping]
        offset_mapping = [x["offset_mapping"] for x in offset_mapping]

        score_mask = [
            [
                -10_000 if offset is None else 0 for offset in seq
            ]
            for seq in offset_mapping
        ]
        score_mask = tf.constant(score_mask, tf.float32)

        start_probs = tf.math.softmax(start_scores + score_mask, axis=-1).numpy()
        end_probs = tf.math.softmax(end_scores + score_mask, axis=-1).numpy()

        answers = []
        for start_prob, end_prob, context, offset in zip(start_probs, end_probs, contexts, offset_mapping):
            start_1 = start_prob.argmax()
            start_1_prob = start_prob.max()
            end_1 = end_prob[start_1:].argmax() + start_1
            end_1_prob = end_prob[start_1:].max()
            answer_prob_1 = start_1_prob + end_1_prob

            end_2 = end_prob.argmax()
            end_2_prob = end_prob.max()
            start_2 = start_prob[:end_2 + 1].argmax()
            start_2_prob = start_prob[:end_2 + 1].max()
            answer_prob_2 = start_2_prob + end_2_prob
            if answer_prob_1 >= answer_prob_2:
                start, end = start_1, end_1
            else:
                start, end = start_2, end_2

            char_index_start = offset[start][0]
            # char_index_start = max(char_index_start - 1, 0)
            char_index_end = offset[end][1]
            answer = context[char_index_start:char_index_end].strip()
            answers.append(answer)
        return answers


class MRCDatasetForDeberta(MRCDataset):
    def _convert_sample_for_training(self, sample: dict):
        title = sample["title"]
        question = sample["question"]
        content = sample["content"]

        context = f"Câu hỏi: {question}. Tiêu đề: {title}. Nội dung: "
        answer_start = sample["answer_start"] + len(context)
        answer_end = answer_start + len(sample["answer"])
        context += content

        inputs = self.tokenizer(
            context,
            truncation=True,
            max_length=self.max_seq_len,
            padding="max_length",
            return_offsets_mapping=True
        )
        start_position = None
        end_position = None
        for i, offset in enumerate(inputs["offset_mapping"]):
            if offset[0] == offset[1] and i > 0:
                break
            if offset[0] >= answer_start and start_position is None:
                start_position = i
            if offset[1] >= answer_end and end_position is None:
                end_position = i
        if start_position is None:
            start_position, end_position = 0, 0
        elif end_position is None:
            end_position = self.max_seq_len - 1
        return {
            "input_ids": inputs["input_ids"],
            "attention_mask": inputs["attention_mask"],
            "labels": [max(start_position - 1, 0), end_position],  # Do start index của Deberta bị lệch 1 token
        }


class MRCDatasetForSeq2SeqModel(MRCDataset):
    is_seq2seq_model = True
    answer_max_len = 256

    def _convert_sample_for_training(self, sample: dict):
        title = sample["title"]
        question = sample["question"]
        content = sample["content"]

        context = f"Câu hỏi: {question}. Tiêu đề: {title}. Nội dung: {content}"
        inputs = self.tokenizer(
            context,
            truncation=True,
            max_length=self.max_seq_len,
            padding="max_length",
        )
        labels = self.tokenizer.convert_tokens_to_ids(self.tokenizer.tokenize(sample["answer"]))
        labels = labels[:self.answer_max_len - 1] + [self.tokenizer.eos_token_id]
        if len(labels) < self.answer_max_len:
            labels += [self.pad_token_label_id] * (self.answer_max_len - len(labels))
        return {
            "input_ids": inputs["input_ids"],
            "attention_mask": inputs["attention_mask"],
            "labels": labels,
        }

    def batch_get_inputs_for_generation(self, data: List[dict]):
        contexts = [
            f"Câu hỏi: {x['question']}. Tiêu đề: {x['title']}. Nội dung: {x['content']}"
            for x in data
        ]
        inputs = self.tokenizer(
            contexts,
            truncation=True,
            max_length=self.max_seq_len,
            padding="max_length",
            return_tensors="tf"
        )
        return {
            "input_ids": inputs["input_ids"],
            "attention_mask": inputs["attention_mask"],
        }

    def batch_decode_model_outputs(self, model_outputs):
        answers = self.tokenizer.batch_decode(model_outputs.numpy(), skip_special_tokens=True)
        return answers
