from vi_mrc.models_map import get_model_version_classes
from vi_mrc.utils import normalize_unicode
from typing import List
import tensorflow as tf
import time
import json
import os
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class TFViMRC:
    def __init__(
        self,
        model_path: str,
        max_seq_len: int = 256,
    ):
        """Hàm tạo."""
        self.version_name = self.get_model_version(model_path)
        self.model_version_map = get_model_version_classes(self.version_name)

        # Tokenizer Object
        self.tokenizer = self.model_version_map.tokenizer_class.from_pretrained(
            model_path, do_lower_case=False, strip_accents=False
        )
        self.dataset_obj = self.model_version_map.dataset_class(
            data_file_path=None,
            tokenizer=self.tokenizer,
            max_seq_len=max_seq_len
        )
        self.model = self.model_version_map.model_class.from_pretrained(model_path)
        if getattr(self.dataset_obj, "is_seq2seq_model", False):
            self.is_seq2seq_model = True
        else:
            self.is_seq2seq_model = False

    @staticmethod
    def get_model_version(model_path):
        with open(os.path.join(model_path, "config.json"), "r") as file:
            config = json.load(file)
        return config["model_version"]

    def batch_infer(
        self,
        data: List[dict],
    ) -> List[str]:
        """Sinh câu trả lời theo batch. Hàm này dùng cho service knowledge_grounded_response_generator.

        Args:
            data (List[dict]): Danh sách các request data
                Mỗi request data là 1 dict:
                {
                    "question": <câu hỏi>,
                    "title": <Tiêu đề của đoạn văn>,
                    "content": <Nội dung của đoạn văn>
                }

        Returns:
            List[str]: Danh sách các câu trả lời tương ứng cho từng request data
        """
        if self.is_seq2seq_model:
            inputs = self.dataset_obj.batch_get_inputs_for_generation(data)
            model_outputs = self.model.generate(
                inputs["input_ids"],
                attention_mask=inputs["attention_mask"],
                max_length=self.dataset_obj.answer_max_len,
                num_beams=2,
            )
            answers = self.dataset_obj.batch_decode_model_outputs(model_outputs)
        else:
            inputs, offset_mapping = self.dataset_obj.batch_get_inputs_for_generation(data)
            start_scores, end_scores = self.model.infer(
                inputs["input_ids"],
                attention_mask=inputs["attention_mask"],
            )
            answers = self.dataset_obj.batch_decode_model_outputs(start_scores, end_scores, offset_mapping)
        return answers


def get_root_path():
    """Trả về đường dẫn đến thư mục gốc."""
    root_path = os.path.dirname(os.path.realpath(__file__))
    return root_path
