#!/bin/bash 

# Nếu chạy evaluate bằng CPU thì đặt CUDA_VISIBLE_DEVICES=-1, chạy bằng GPU thì đặt CUDA_VISIBLE_DEVICES=0
CUDA_VISIBLE_DEVICES=-1 python3 vi_mrc/evaluate.py \
    --model_path mounts/xlm_roberta_base_mrc_lr5e5_e10-best-val \
    --data_path mounts/ViQuad/ViQuAD_v1_test.json \
    --batch_size 2 \
    --max_seq_len 512 \
    --output_file mounts/model_outputs.json
