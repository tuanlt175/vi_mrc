#!/bin/bash 

# run train
CUDA_VISIBLE_DEVICES=0 python3 vi_mrc/run_train.py \
    --model_name_or_path xlm-roberta-base \
    --model_version xlmroberta-mrc \
    --output_dir mounts/models/xlm_roberta_base_mrc_lr5e5_e10 \
    --data_train mounts/data/v1_train_ViQuAD.json \
    --data_dev mounts/data/v1_dev_ViQuAD.json \
    --save_steps 0 \
    --learning_rate 5e-5 \
    --cache_data_size 2048 \
    --max_seq_len 512 \
    --per_device_train_batch_size 32 \
    --per_device_eval_batch_size 128 \
    --num_train_epochs 10
